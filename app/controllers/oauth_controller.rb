# frozen_string_literal: true

class OauthController < ApplicationController
  def shutdown
    current_user.yandex_id = nil if params[:oauth_client] == 'yandex'
    current_user.vk_id = nil if params[:oauth_client] == 'vkontakte'
    current_user.fb_id = nil if params[:oauth_client] == 'facebook'
    current_user.google_id = nil if params[:oauth_client] == 'google'
    current_user.mail_id = nil if params[:oauth_client] == 'mail'
    current_user.save

    redirect_to_last_page

  end

  def result_auth_user(options)

    options[:email] = options[:email].downcase

    if user_signed_in?
      id = options[:id]
      current_user.vk_id = id if (options[:oauth_client] == 'vk') && !User.exists?(vk_id: id)
      current_user.fb_id = id if (options[:oauth_client] == 'fb') && !User.exists?(fb_id: id)
      current_user.google_id = id if (options[:oauth_client] == 'google') && !User.exists?(google_id: id)

      current_user.save(validate: false)
    else
      create_user = false
      if User.exists?( "#{options[:oauth_client]}_id" => options[:id] )
        new_user = User.find_by( "#{options[:oauth_client]}_id" => options[:id] )
      else
        if User.exists?(email: options[:email])
          new_user = User.find_by(email: options[:email])
        else
          new_user = User.new
          create_user = true
        end
      end

      new_user["#{options[:oauth_client]}_id"] = options[:id]
      if create_user == true
        new_user.name = "#{options[:last_name]} #{options[:name]}"
        new_user.email = options[:email]
        new_user.password = Devise.friendly_token[0, 20]
      end
      new_user.save(validate: false)

      sign_in new_user, event: :authentication
    end

    redirect_to_last_page
  end


  def google
    access_token = JSON.parse(
      Net::HTTP.post_form(
        URI.parse('https://accounts.google.com/o/oauth2/token'),
        'grant_type' => 'authorization_code',
        'client_id' => '668728170578-5b7qr0fuml2eajfjfq67ictptk24rrln.apps.googleusercontent.com',
        'client_secret' => 'W2BxwRGs_VZRwk1aphEFVVpX',
        'redirect_uri' => 'https://plane-sale.com/oauth/google/',
        'code' => params[:code]
      ).body
    )['access_token']

    if access_token.present?
      data = JSON.parse(
        Net::HTTP.get(
          URI.parse("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=#{access_token}")
        )
      )
      result_auth_user(
        id: data['id'],
        name: data['given_name'],
        last_name: data['family_name'],
        oauth_client: 'google',
        email: data['email']
      )
    else
      redirect_to_last_page
    end
  end

  def facebook
    access_token = JSON.parse(
      Net::HTTP.post_form(
        URI.parse('https://graph.facebook.com/v3.3/oauth/access_token?'),
        'client_id' => '331273744424433',
        'client_secret' => '8eb5f36db14041d15f2ecddb7a5e1bf0',
        'redirect_uri' =>
        'https://plane-sale.com/oauth/facebook/',
        'code' => params[:code]
      ).body
    )['access_token']

    if access_token.present?

      data = JSON.parse(
        Net::HTTP.post_form(
          URI.parse('https://graph.facebook.com/me?'),
          'fields' => 'id,first_name,last_name,email,picture',
          'oauth_token' => access_token
        ).body
      )

      result_auth_user(
        id: data['id'],
        name: data['first_name'],
        last_name: data['last_name'],
        oauth_client: 'fb',
        email: data['email']
      )
    else
      redirect_to_last_page
    end
  end

  def vkontakte
    token_data = JSON.parse( # получаем токен
      Net::HTTP.post_form(
        URI.parse('https://oauth.vk.com/access_token?'),
        'client_id' => '7274383',
        'client_secret' => 'RN0BfcfMOMKwXYsTdVzU',
        'redirect_uri' => 'https://plane-sale.com/oauth/vkontakte/',
        'code' => params[:code]
      ).body
    )

    if token_data['access_token'].present? # если токен получен
      email = token_data['email']
      data = JSON.parse( # заправшиваем инфу о пользователей
        Net::HTTP.post_form(
          URI.parse('https://api.vk.com/method/users.get'),
          'v' => '5.21',
          'user_ids' => token_data['user_id'],
          'access_token' => token_data['access_token']
        ).body
      )['response'][0]

      result_auth_user( # передаем инфу на авторизацию
        id: data['id'],
        name: data['first_name'],
        last_name: data['last_name'],
        oauth_client: 'vk',
        email: email
      )
    else
      redirect_to_last_page
    end
  end
end
