class ApplicationController < ActionController::Base
  
  skip_before_action :verify_authenticity_token
  
  
  before_action :check_user_id?

	def check_user_id?

    if cookies[:user_id].blank?
      if user_signed_in?
        if current_user.user_id.present?
          cookies[:user_id]=current_user.user_id
        else
          current_user.user_id=ApplicationController.create_user_id
          current_user.save(validate: false)
          cookies[:user_id]=current_user.user_id
        end
        
      else
        cookies[:user_id]=ApplicationController.create_user_id
      end
    else
      if user_signed_in?
        if current_user.user_id.blank?
          current_user.user_id=cookies[:user_id]
          current_user.save(validate: false)
        end
      end
    end
      
	end
  
  def self.create_user_id
    
    hash=md5 = Digest::MD5.new
    code="#{rand(0..999)}_#{Time.new}"
    hash=md5.hexdigest code
    
    return hash
    
  end
  
  
  
  def upload_file
    
    
    
    render plain: 'ok'
  end
  
  
end
