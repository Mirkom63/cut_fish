//= require react


class Upload extends React.Component {

  constructor(props) {
    super(props);
    message_root_app=this;
    this.state = {
      user_id: false, //ID авторизованного пользователя

    }
  }

  componentDidMount(){
  }
  
  render(){
    return(
      <div>test</div>
    );
  }
  
}

ReactDOM.render( <Upload user_id={$('.js-user-id').val()}/>, document.getElementById('upload_root'));


$(document).ready(function(){
  
  $('.pop_up_center_error_button_open_close').click(function(){
    var description=$(this).parent().find('.pop_up_center_error_button_open_close_info');
    
    if(description.css('display')=='block'){
      description.hide();
      $(this).removeClass('open');
    }else{
      description.show();
      $(this).addClass('open');
    }
  });
  
  $('.pop_up_center_close').click(function(){
    $(this).closest('.pop_up').hide();
    $('body').css('overflow','auto')
  });
  
  $('.upload_history_list').scrollbar();
  
  $('.upload_history_list input[type=checkbox]').checkboxradio();
  
});