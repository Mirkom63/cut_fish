// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//=require plugin/ui
//=require plugin/jquery.scrollbar.min.js

$(document).ready(function(){
  
  // $.ajax({
  //   type     :'POST',
  //   cache    : false,
  //   async    : true,
  //   url  : 'https://api.cut.fish/rest/test',
  //   success  : function(response) {
  //     console.log('check');
  //   }
  // });
  
  
  var socket = io.connect('https://api.cut.fish:9292');
  socket.emit('join', {room_id: "notification"});

  socket.on('event', function(data){
    console.log("OLALA! I take this data: "+data);
  });

  
  ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
    [document.getElementsByClassName('block_upload_selector')].forEach(function(){
      this.addEventListener(eventName, preventDefaults, false);
    });
    
  })

  function preventDefaults (e) {
    e.preventDefault()
    e.stopPropagation()
  }


  var parent;
  
  $('.block_upload_selector')
      .on('drop', function (e) {
      
      var files = e.originalEvent.dataTransfer.files;
    
    
      var file = files[0];
      parent=$(this).parent();
      
      uploadFile(file);
      
      
  });
  
  $('.block_upload_button_input').change(function(){
    var files = document.getElementsByClassName('block_upload_button_input')[0].files;
    parent=$(this).parent().parent().parent();
    
    uploadFile(files[0]);
    
  });
  
  
  function uploadFile(file){
    
    var formdata = new FormData();
    formdata.append("file", file);
    formdata.append("userId", '2f54e91b-f550-40a6-b979-a5448c75fa86');
    
    
    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", progressHandler, false);
            xhr.addEventListener("progress", progressHandler, false);
            return xhr;
        },
        url: 'https://api.cut.fish/rest/upload_video',
        type: 'POST',
        data: formdata,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        enctype: 'multipart/form-data',
        success: function (returndata) {
            alert(returndata);
        }
    });
      
    // $.ajax({
    //   url: 'https://api.cut.fish/rest/upload_video',
    //   type: 'POST',
    //   data: formdata,
    //   cache: false,
    //   dataType: 'json',
    //   processData: false,
    //   contentType: false,
    //   success: function( respond, textStatus, jqXHR ){
    // 
    //     console.log(respond);
    // 
    //   },
    //   error: function( jqXHR, textStatus, errorThrown ){
    // 
    //     alert('Не готово!');
    //   }
    // });
    // 
    // set_yellow=false;
    // set_green=false;
    // 
    // parent.find('.block_upload_selector').hide();
    // parent.find('.block_upload_loader').show();
    // parent.find('.block_upload_loader_line').width('100%');
    // parent.find('.block_upload_loader_comment').text('Uploading...');
    // 
    // console.log(file.name+" | "+file.size+" | "+file.type);
    // var formdata = new FormData();
    // formdata.append("file", file);
    // formdata.append("userId", '2f54e91b-f550-40a6-b979-a5448c75fa86');
    // var ajax = new XMLHttpRequest();
    // ajax.upload.addEventListener("progress", progressHandler, false);
    // ajax.addEventListener("load", completeHandler, false);
    // ajax.addEventListener("error", errorHandler, false);
    // ajax.addEventListener("abort", abortHandler, false);
    // ajax.open("POST", "https://api.cut.fish/rest/upload_video", true);
    // ajax.send(formdata);
    
    // setTimeout(function(){
    //   set_yellow=false;
    // },2000);
    // setTimeout(function(){
    //   set_green=false;
    // },4000);
  }

  var set_yellow=true;
  var set_green=true;
  function progressHandler(event) {
    var percent = (event.loaded / event.total) * 100;
    if(percent>20 && set_yellow==false){
      set_yellow=true;
      parent.find('.block_upload_loader_comment').text('Om-Nom-nom...');
      parent.find('.block_upload_loader_line_color').css('background','#FCD200');
    }
    if(percent>60 && set_green==false){
      set_green=true;
      parent.find('.block_upload_loader_comment').html('Background is my <br>favorite dish!');
      parent.find('.block_upload_loader_line_color').css('background','#06D6A0');
    }
    percent=percent*0.85;
    $('.block_upload_loader_line').width((100-percent)+'%');
  }

  function completeHandler(event) {
  }

  function errorHandler(event) {
    console.log(event);
  }

  function abortHandler(event) {
    
  }


});