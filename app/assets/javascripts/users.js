$(document).ready(function(){
  
  $('.users_form input[type=checkbox]').checkboxradio();
  
  
  //Auth START
  function check_auth(){

    var email=$('.js-form-auth').find('input[name="user[email]"]').val();
    var password=$('.js-form-auth').find('input[name="user[password]"]').val();

    if(email.length>0 && password.length>0){
      $('.js-form-auth button').prop('disabled',false);
      $('.js-form-auth button').html('Sign up');
    }else{
      $('.js-form-auth button').attr('disabled','disabled');
    }
  }

  $('.js-form-auth input').on("keyup",function(){
    $('.js-form-auth .users_form_error').hide();
  });
  $('.js-form-auth input').on("change keyup blur input",function(){
    check_auth();
  });
  
  
  $('.js-form-auth').submit(function(e){
    var form=$(this);
    e.preventDefault();
  
    $('.js-form-auth button').html('<div class="button_loader">\
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\
          <path d="M18.0556 10C18.0556 14.4444 14.4444 18.0556 10 18.0556C5.55556 18.0556 1.94444 14.4444 1.94444 10C1.94444 5.55556 5.55556 1.94444 10 1.94444V0C4.47222 0 0 4.47222 0 10C0 15.5278 4.47222 20 10 20C15.5278 20 20 15.5278 20 10H18.0556Z" fill="white"/>\
        </svg>\
      </div>');
  
    var email=$('.js-form-auth').find('input[name="user[email]"]').val();
    var password=$('.js-form-auth').find('input[name="user[password]"]').val();
  
  
    $.ajax({
        type     :'POST',
        data: $('.js-form-auth').serialize(),
        url  : '/users/sign_in',
        success  : function(response) {
          form.unbind();
          form.submit();
        },
        error : function(error){
          if(error.responseText=='Invalid Email or password.') {
            $('.users_form_error').text("Invalid Email or password.").show();
            $('.js-form-auth button').html('Sign up');
          }else{
            form.unbind();
            form.submit();
          }
  
        }
    });
  
  
  });
  //Auth END
  
  
  
  //Registration START


  function check_reg(){

    $('.js-form-reg .users_form_error').hide();

    var error="";

    var email=$('.js-form-reg').find('input[name="user[email]"]').val();
    var password=$('.js-form-reg').find('input[name="user[password]"]').val();
    var password_confirm=$('.js-form-reg').find('input[name="user[password_confirmation]"]').val();

    if(password.length<6 && password.length>0){
      error+='The password must be more than 5 characters long<br>';
    }

    if(password.length>5 && password_confirm.length>0 && (password!=password_confirm) ){
      error+="Passwords don't match<br>";
    }

    if(
      email.length>3 &&
      password.length>5 &&
      password_confirm.length>5 &&
      (password==password_confirm) &&
      $('.js-form-reg').find('input[name="conditions"]').prop('checked')==true
    ){
      $('.js-form-reg button').prop('disabled',false);
      $('.js-form-reg .users_form_error').hide();
      $('.js-form-reg button').html('Sign up');
    }else{
      $('.js-form-reg button').attr('disabled','disabled');
    }
    
    if(error.length>0){
      $('.js-form-reg .users_form_error').html(error).show();
    }else{
      $('.js-form-reg .users_form_error').html("").hide();
    }

  }

  $('.js-form-reg input').on("change keyup blur input",function(){
    $('.js-form-reg .js-error-password').hide();
    check_reg();
  });

  

  $('.js-form-reg').submit(function(e){
    e.preventDefault();

    $('.js-form-reg button').html('<div class="button_loader">\
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\
          <path d="M18.0556 10C18.0556 14.4444 14.4444 18.0556 10 18.0556C5.55556 18.0556 1.94444 14.4444 1.94444 10C1.94444 5.55556 5.55556 1.94444 10 1.94444V0C4.47222 0 0 4.47222 0 10C0 15.5278 4.47222 20 10 20C15.5278 20 20 15.5278 20 10H18.0556Z" fill="white"/>\
        </svg>\
      </div>');

    $.ajax({
        type     :'POST',
        cache    : false,
        data: $('.js-form-reg').serialize(),
        url  : '/users',
        success  : function(response) {
          console.log(response);
          // if(response.error==undefined){
          //   window.location='/';
          // }else{
          //   $('.js-form-reg .reg_block_form_error_list').html(response.error).show();
          //   $('.js-form-reg button').html('Sign up');
          // }

        },
        error: function(jqxhr, status, errorMsg) {
        }
    });

  });
  //Registration END
  
  
});