class AddToUserSocialId < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :fb_id, :string
    add_column :users, :vk_id, :string
    add_column :users, :google_id, :string
  end
end
