Rails.application.routes.draw do
  
  devise_for :users
  
  get '/log_in' => 'users#log_in'
  get '/sign_up' => 'users#sign_up'
  
  get '/upload' => 'upload#index'
  
  post '/upload_file' => 'application#upload_file'
  
  root 'index#index'
end
